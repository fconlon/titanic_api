from flask import Blueprint, request

from .helpers import prefix
from .models import Passenger

all_data_view = Blueprint('all_data', __name__,)

@all_data_view.route(prefix('/all_data'))
def all_data():
    filter_expression_map = {
        'class1': (Passenger.passenger_class==1),
        'class2': (Passenger.passenger_class==2),
        'class3': (Passenger.passenger_class==3),
        'female': (Passenger.gender=='female'),
        'male': (Passenger.gender=='male'),
        'lived': (Passenger.survived==True),
        'died': (Passenger.survived==False),
        'child': (Passenger.age<18),
        'adult': (Passenger.age>=18)
    }
    filters = request.args['filters'].split(',')
    if filters[0]:
        filters = [filter_expression_map[filter] for filter in filters]
    else:
        filters = []
    cylinder_counts = {
        'class': {
            1: Passenger.query.filter(Passenger.passenger_class==1, *filters).count(),
            2: Passenger.query.filter(Passenger.passenger_class==2, *filters).count(),
            3: Passenger.query.filter(Passenger.passenger_class==3, *filters).count(),
        },
        'female': {
            1: Passenger.query.filter(Passenger.passenger_class==1, Passenger.gender=='female', *filters).count(),
            2: Passenger.query.filter(Passenger.passenger_class==2, Passenger.gender=='female', *filters).count(),
            3: Passenger.query.filter(Passenger.passenger_class==3, Passenger.gender=='female', *filters).count(),
        },
        'male': {
            1: Passenger.query.filter(Passenger.passenger_class==1, Passenger.gender=='male', *filters).count(),
            2: Passenger.query.filter(Passenger.passenger_class==2, Passenger.gender=='male', *filters).count(),
            3: Passenger.query.filter(Passenger.passenger_class==3, Passenger.gender=='male', *filters).count(),
        },
        'lived': {
            1: Passenger.query.filter(Passenger.passenger_class==1, Passenger.survived==True, *filters).count(),
            2: Passenger.query.filter(Passenger.passenger_class==2, Passenger.survived==True, *filters).count(),
            3: Passenger.query.filter(Passenger.passenger_class==3, Passenger.survived==True, *filters).count(),
        },
        'died': {
            1: Passenger.query.filter(Passenger.passenger_class==1, Passenger.survived==False, *filters).count(),
            2: Passenger.query.filter(Passenger.passenger_class==2, Passenger.survived==False, *filters).count(),
            3: Passenger.query.filter(Passenger.passenger_class==3, Passenger.survived==False, *filters).count(),
        },
        'child': {
            1: Passenger.query.filter(Passenger.passenger_class==1, Passenger.age<18, *filters).count(),
            2: Passenger.query.filter(Passenger.passenger_class==2, Passenger.age<18, *filters).count(),
            3: Passenger.query.filter(Passenger.passenger_class==3, Passenger.age<18, *filters).count(),
        },
        'adult': {
            1: Passenger.query.filter(Passenger.passenger_class==1, Passenger.age>=18, *filters).count(),
            2: Passenger.query.filter(Passenger.passenger_class==2, Passenger.age>=18, *filters).count(),
            3: Passenger.query.filter(Passenger.passenger_class==3, Passenger.age>=18, *filters).count(),
        }
    }
    return cylinder_counts
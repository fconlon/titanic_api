# import base python packages first
import os

# import pypi packages next
from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS
from sqlalchemy import URL

# import local pacakges last
from titanic.models import db, migrate
from titanic.all_data import all_data_view

load_dotenv()

def create_app():
    app = Flask(__name__)
    database_url = URL.create(
        drivername='postgresql',
        username=os.getenv('DB_USER'),
        password=os.getenv('DB_PASSWORD'),
        host=os.getenv('DB_HOST'),
        port=os.getenv('DB_PORT'),
        database=os.getenv('DB'),
    )
    app.config['SQLALCHEMY_DATABASE_URI'] = database_url
    app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')

    # initialize extensions in alphabetical order for ease of maintenance
    db.init_app(app)
    CORS(app, resources={
            r'/titanic/api/v1*': {
                'origins': [
                    'https://flconlon.com',
                    'https://www.flconlon.com',
                    'http://localhost:5173',
                ]
            }
        }
    )
    migrate.init_app(app, db)

    # register the views in alphabetical order for ease of maintenance
    app.register_blueprint(all_data_view)
    
    return app
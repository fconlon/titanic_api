flask
flask-cors
flask-migrate
flask-sqlalchemy
gunicorn
marshmallow
psycopg2
python-dotenv
